/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package org.vertexprize.statemachine.gs.papyrus;

import lombok.extern.slf4j.Slf4j;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.StateContext.Stage;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.listener.StateMachineListenerAdapter;
import org.springframework.stereotype.Component;

/**
 *  Прослушивание и отображение на экране лога выполнения машины состояний
 * 
 * @author vaganovdv
 */
@Slf4j
@Component
public class StateMachineLogListener extends   StateMachineListenerAdapter<String, String>{

   
    
    
    
    @Override
    public void stateContext(StateContext<String, String> stateContext) {
        
            String event = stateContext.getEvent();
            String source = "";
            String target = "";
            String error = "";
            if (stateContext.getSource() != null) {
                source = stateContext.getSource().getId();
            }
            if (stateContext.getTarget() != null) {
                target = stateContext.getTarget().getId();
            }
        
        
        if (stateContext.getStage() == Stage.STATEMACHINE_START) {
            log.info(String.format("%-50s %s", "[" + stateContext.getStateMachine().getId()+"]", "Машина состояний работает"));
        }

        if (stateContext.getStage() == Stage.STATEMACHINE_STOP) {
            log.info(String.format("%-50s %s", "[" + stateContext.getStateMachine().getId()+"]" , "Машина состояний остановлена"));
        }

        if (stateContext.getStage() == Stage.EVENT_NOT_ACCEPTED) {            
            log.warn(String.format("%-50s %s", "[" + stateContext.getStateMachine().getId()+"]", "Событие не принято: " + event));
        }

        if (stateContext.getStage() == Stage.STATE_CHANGED) {           
            log.info(String.format("%-50s %s","[" + stateContext.getStateMachine().getId()+"]", "Смена состояния: [" + source + "] ==> [" + target + "] событие: [" + event + "]"));
        }
        
//        if (stateContext.getStage() == Stage.STATE_ENTRY) {
//            log.info(String.format("%-50s %s","[" + stateContext.getStateMachine().getId()+"]", "Вход в состояние: [" + target + "]"));            
//            if (stateContext.getTarget().getId().equals("ERROR")) {
//                error = (String) stateContext.getExtendedState().getVariables().get("error");
//                log.info(String.format("%-50s %s","[" + stateContext.getStateMachine().getId()+"]", "Ошибка: "+error));            
//            }            
//        }
//        if (stateContext.getStage() == Stage.STATE_EXIT) {
//            log.info(String.format("%-50s %s","[" + stateContext.getStateMachine().getId()+"]", "Выход из состояния: [" +source + "]"));                               
//        }
        

    }

       
    @Override
    public void stateMachineError(StateMachine<String, String> stateMachine, Exception ex) {
       StringBuilder sb = new StringBuilder();
       sb.append("\n");
       sb.append("Описание ошибки: "+ex.toString());
       sb.append("\n");
       log.error(String.format("%-50s %s", "[" + stateMachine.getId()+"] Ошибка машины состояний", "id = " + stateMachine.getId())+sb.toString());
    }

    
    
    
}
