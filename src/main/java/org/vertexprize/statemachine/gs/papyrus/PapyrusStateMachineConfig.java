/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package org.vertexprize.statemachine.gs.papyrus;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.action.Action;
import org.springframework.statemachine.config.EnableStateMachine;
import org.springframework.statemachine.config.StateMachineConfigurerAdapter;
import org.springframework.statemachine.config.builders.StateMachineConfigurationConfigurer;
import org.springframework.statemachine.config.builders.StateMachineModelConfigurer;
import org.springframework.statemachine.config.model.StateMachineModelFactory;
import org.springframework.statemachine.uml.UmlStateMachineModelFactory;

/**
 *  Проеект и конфигурация машины состояний c использованием Eclipse Papyrus
 * 
 * @author developer
 */
@Configuration
@EnableStateMachine
@Slf4j
public class PapyrusStateMachineConfig  extends StateMachineConfigurerAdapter<String, String>{
 
    @Autowired
    private StateMachineLogListener stateMachineLogListener; 
    
    /**
     * Общая конфигурация машины состояний 
     * 
     * 
     * @param config
     * @throws Exception 
     */
    @Override
    public void configure(StateMachineModelConfigurer<String, String> model) throws Exception {
        model.withModel()
                .factory(modelFactory());
    }

    @Override
    public void configure(StateMachineConfigurationConfigurer<String, String> config)
            throws Exception {
        config
                .withConfiguration()
                .autoStartup(false)
                .machineId("state-machine")
                .listener(stateMachineLogListener);
    }

    
      @Bean
    public StateMachineModelFactory<String, String> modelFactory() {
       return new UmlStateMachineModelFactory("classpath:/uml/state-machine-papyrus/state-machine-papyrus.uml");
    }

    
    
    @Bean
    public Action<String, String> sendMessage() {
        return new Action<String, String>() {
            @Override
            public void execute(StateContext<String, String> context) {                
                context.getStateMachine().getState().getId();
                
                log.info("ACTION ===>  Выполнение действия [sendMessage],  событие: " + context.getEvent() + ", состяние: " + context.getTarget().getId());
            }
        };
    }

   
    
    
}
