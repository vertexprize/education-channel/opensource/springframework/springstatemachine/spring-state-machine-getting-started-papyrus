/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package org.vertexprize.statemachine.gs.papyrus.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.StateMachineEventResult;
import org.springframework.statemachine.state.State;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import reactor.core.publisher.Flux;

/**
 *
 * @author developer
 */
@Controller
@Slf4j
public class StateMachineController {
    
    @Autowired
    private StateMachine<String, String> stateMachine;
    
    @RequestMapping(value = "/state", method = RequestMethod.GET, produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<?> state() {        
        if (stateMachine != null) {                        
            State<String, String> state = stateMachine.getState();
            return ResponseEntity.ok("Состояние: " + state.getId());
        } else {
            return ResponseEntity.ok("Статус: машина не существует");            
        }        
        
    }
    
    @RequestMapping(value = "/{event}", method = RequestMethod.GET, produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<?> sendEvent(@PathVariable("event") String event) {
        
        String responce = "";
        log.info("Поступило событие: " + event);
        Message<String> m = MessageBuilder.withPayload(event).build();
        
        if (stateMachine != null) {
            log.info("Отправка события [" + event + "] в машину состояний ");
            
            Flux<StateMachineEventResult<String, String>> results = stateMachine.sendEvents(Flux.just(m));
            results.subscribe();
            
            State<String, String> state = stateMachine.getState();
            responce = "Событие: [" + event + "] ==> состояние: [" + state.getId() + "]";            
            return ResponseEntity.ok(responce);
        } else {
            return ResponseEntity.ok("Статус: машина не существует");            
        }
        
    }

}
