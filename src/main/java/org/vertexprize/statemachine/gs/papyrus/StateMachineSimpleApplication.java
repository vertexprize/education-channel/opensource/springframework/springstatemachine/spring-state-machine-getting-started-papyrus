package org.vertexprize.statemachine.gs.papyrus;

import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.StateMachineEventResult;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@SpringBootApplication
@Slf4j
public class StateMachineSimpleApplication {

    public static void main(String[] args) {
        SpringApplication.run(StateMachineSimpleApplication.class, args);
    }

    @Autowired
    private StateMachine<String, String> stateMachine;

    
    @PostConstruct
    public void start() {

       log.info("Старт  машины: " + stateMachine.getId());
        
        Mono<Void> startReactively = stateMachine.startReactively();
        startReactively.block();
        
//        Message<String> m0 = MessageBuilder.withPayload("E0").build();
//        Message<String> m1 = MessageBuilder.withPayload("E1").build();
//        Message<String> m2 = MessageBuilder.withPayload("E2").build();
//         
//        log.info("Отправка сообщений: " + m0.getPayload()+", "+m1.getPayload()+", "+m2.getPayload() );
//        
//        Flux<StateMachineEventResult<String, String>> results                
//                = stateMachine.sendEvents(Flux.just(m0, m1, m2));
//
//        results.subscribe();       
        
        
    }

}
